# DT017A_Lab
The assignment of course "Distributed System Part I".

Basically, it's a chat application with a distributed manner. There are 2 communication methods: total order and causal order. Meawhile, a chat group is able to elect a new leader when the current leader is down.

It's just an assignment. I don't have much experience with Java. The design of architecture is a little 'tricky' and the coding style is somewhat ugly. Anyway, it had passed the examination and this respository is sort of a record for myself.

