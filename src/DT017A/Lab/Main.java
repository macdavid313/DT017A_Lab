package DT017A.Lab;

import DT017A.Lab.View.chooseOrderWindow;

public class Main {

    public static void main(String[] args) {
        float dropRate;
        if (args.length == 0){
            dropRate = 0;
        }
        else {
            dropRate = (float) (Integer.parseInt(args[0]) / 100.0);
        }
        Client self = Client.getInstance();
        self.setDropRate(dropRate);
        new chooseOrderWindow();
    }

}
