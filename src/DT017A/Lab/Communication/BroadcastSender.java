package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.Message.BroadcastMessage;
import DT017A.Lab.Model.Message.GeneralMulticastMessage;
import DT017A.Lab.Model.Message.Message;

import java.io.IOException;
import java.net.*;

/**
 * Created by tianyu on 01/01/15.
 */

public class BroadcastSender {
    private DatagramSocket socket;
    private InetAddress address;
    private static BroadcastSender instance = new BroadcastSender();

    public static BroadcastSender getInstance(){
        return instance;
    }

    private BroadcastSender(){
        try {
            this.address = InetAddress.getByName("255.255.255.255");
            this.socket = new DatagramSocket();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void findLeader(){
        Message message = new Message();
        GeneralMulticastMessage generalMulticastMessage = new GeneralMulticastMessage();
        // BroadcastMessage includes groupname and (Member)self
        generalMulticastMessage.setContent(new BroadcastMessage());
        message.setId(Client.getInstance().getId()); // here, id = -1, which is not allocated by leader
        message.setAction("delivery");
        message.setContent(generalMulticastMessage);
        this.send(message);
    }

    public void send(Message message){
        byte[] data = message.serialize();
        DatagramPacket packet =
                new DatagramPacket(data, data.length, address, Client.BROADCAST_RECEIVE_PORT);
        try {
            this.socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
