package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.*;
import DT017A.Lab.Model.Message.*;
import DT017A.Lab.orderType;

import java.util.Map;

/**
 * Created by tianyu on 01/01/15.
 */
public class GeneralMulticast {
    private static GeneralMulticast instance = new GeneralMulticast();
    private ReliableUnicastSender sender;

    private GeneralMulticast(){
        sender = ReliableUnicastSender.getInstance();
    }

    public static GeneralMulticast getInstance(){
        return instance;
    }

    public void send(IMessage content){
        GeneralMulticastMessage message = new GeneralMulticastMessage();
        message.setContent(content);
        this.multicast(message);
    }

    private void multicast(GeneralMulticastMessage data){
        Client self = Client.getInstance();
        MemberIndexer memberIndexer = MemberIndexer.getInstace();
        Map<Integer, Member> members = memberIndexer.getAllMembers();
        for(Map.Entry<Integer, Member> entry : members.entrySet()){
            if (entry.getKey() == self.getId() &&
                    entry.getValue().getUsername().equals(self.getUsername())){
                continue;
            }
            Message message = new Message();
            message.setContent(data);
            message.setAction("delivery");
            message.setId(Client.getInstance().getId());
            sender.send(message, entry.getValue());
        }
    }

    public synchronized void delivery(GeneralMulticastMessage message){
        Client self = Client.getInstance();
        MemberIndexer memberIndexer = MemberIndexer.getInstace();
        if (message == null){
            System.out.println("Content is NIL!");
            return;
        }
        else {
            IMessage content = message.getContent();
            if (content instanceof GroupInfoMessage){
                //System.out.println(((GroupInfoMessage) content).getAction() + " from " + ((GroupInfoMessage) content).getSenderId());
                this.handler_GroupInfoMessage((GroupInfoMessage)content);
            }
            if (content instanceof TotalOrderMessage){
                TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                totalOrderMulticast.delivery(content);
            }
            if (content instanceof CausalOrderMessage){
                CausalOrderMulticast causalOrderMulticast = CausalOrderMulticast.getInstance();
                causalOrderMulticast.delivery(content);
            }
        }
    }

    private void handler_GroupInfoMessage(GroupInfoMessage groupInfoMessage){
        Client self = Client.getInstance();
        MemberIndexer memberIndexer = MemberIndexer.getInstace();
        //System.out.println(groupInfoMessage.getAction() + " from sender: " + groupInfoMessage.getSenderId());
        if (groupInfoMessage.getAction().equals("Broadcast_Leaving")){
            int your_leader = groupInfoMessage.getYour_leader();
            int senderId = groupInfoMessage.getSenderId();
            if (your_leader != -1 && memberIndexer.getLeader() == senderId){
                memberIndexer.setLeader(your_leader);
            }

            memberIndexer.removeMember(senderId);

            if (self.getOrderType() == orderType.TotalOrder){
                TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                totalOrderMulticast.setNextExpectedSequence();
            }
            if (self.isLeader()){
                TotalOrderSequencer totalOrderSequencer = TotalOrderSequencer.getInstance();
                totalOrderSequencer.setNextMessageSequence();
            }
            //memberIndexer.printCurrentTable();
            //System.out.println("Message Delivered from: " + senderId + ", Leader: " + memberIndexer.getLeader());
        }
        else if (groupInfoMessage.getAction().equals("LEADER_IS_DOWN")){
            int new_leader = groupInfoMessage.getYour_leader();
            if (memberIndexer.getLeader() == new_leader) return;
            memberIndexer.removeMember(memberIndexer.getLeader());
            memberIndexer.setLeader(new_leader);
            if (self.getOrderType() == orderType.TotalOrder){
                TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                totalOrderMulticast.setNextExpectedSequence();
            }
            if (self.isLeader()){
                TotalOrderSequencer totalOrderSequencer = TotalOrderSequencer.getInstance();
                totalOrderSequencer.setNextMessageSequence();
            }
            //if (self.getId() )
            //memberIndexer.printCurrentTable();
        }
    }
}
