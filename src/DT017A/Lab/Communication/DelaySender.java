package DT017A.Lab.Communication;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.TimerTask;

/**
 * Created by tianyu on 31/12/14.
 */
public class DelaySender extends TimerTask {
    private DatagramSocket socket;
    private DatagramPacket packet;

    public DelaySender(DatagramSocket socket, DatagramPacket packet){
        this.socket = socket;
        this.packet = packet;
    }

    @Override
    public void run() {
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
