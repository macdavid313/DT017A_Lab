package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.MemberIndexer;
import DT017A.Lab.Model.Message.CausalOrderMessage;
import DT017A.Lab.Model.Message.IMessage;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tianyu on 04/01/15.
 */
public class CausalOrderMulticast {
    private static CausalOrderMulticast instance = new CausalOrderMulticast();
    private GeneralMulticast generalMulticast;
    private Integer[] vectorClock;
    private List<CausalOrderMessage> holdbackQueue;
    private final Object mutex;
    private Client self = Client.getInstance();

    private CausalOrderMulticast(){
        int groupSize = MemberIndexer.getInstace().countMembers();
        vectorClock = new Integer[groupSize];
        for (int i = 0; i < groupSize; i++) {
            vectorClock[i] = 0;
        }
        holdbackQueue = new LinkedList<CausalOrderMessage>();
        generalMulticast = GeneralMulticast.getInstance();
        mutex = new Object();
    }

    public static CausalOrderMulticast getInstance() {
        return instance;
    }

    public void send(String chatText){
        synchronized (mutex){
            CausalOrderMessage causalOrderMessage;
            int selfId = Client.getInstance().getId();
            this.vectorClock[selfId] += 1;
            causalOrderMessage = new CausalOrderMessage();
            causalOrderMessage.setSource(selfId);
            causalOrderMessage.setText(chatText);
            causalOrderMessage.setVectorClock(this.vectorClock);
            this.generalMulticast.send(causalOrderMessage);
            //this.holdbackQueue.add(causalOrderMessage);
            //this.delivery(causalOrderMessage);
            self.setMsg_pool(causalOrderMessage.getText());
        }
    }

    public void delivery(IMessage message){
        synchronized (mutex){
            CausalOrderMessage causalOrderMessage = (CausalOrderMessage)message;
            this.holdbackQueue.add(causalOrderMessage);

            boolean flag = true;
            List<CausalOrderMessage> deleteQueue = new LinkedList<CausalOrderMessage>();

            while (flag){
                flag = false;
                boolean isReady = true;
                int sourceId = -1;
                for (CausalOrderMessage msg : holdbackQueue){
                    Integer[] msgVectorClock = msg.getVectorClock();
                    sourceId = msg.getSource();
                    isReady = true;
                    if (msgVectorClock[sourceId] == this.vectorClock[sourceId] + 1){
                        for (int i = 0; i < msgVectorClock.length; i++) {
                            if (i != sourceId && msgVectorClock[i] > this.vectorClock[i]){
                                // not ready to deliver a message
                                isReady = false;
                                break;
                            }
                        }
                    } else {
                        // not ready to deliver a message
                        isReady = false;
                    }
                    if (isReady){
                        String text = msg.getText();
                        self.setMsg_pool(text);
                        deleteQueue.add(msg);
                        this.vectorClock[sourceId] += 1;
                        flag = true;
                    }
                }
            }

        }
    }
}
