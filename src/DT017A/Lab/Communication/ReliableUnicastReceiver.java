package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.Member;
import DT017A.Lab.Model.MemberIndexer;
import DT017A.Lab.Model.Message.GroupInfoMessage;
import DT017A.Lab.Model.Message.Message;
import DT017A.Lab.orderType;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by tianyu on 31/12/14.
 */
public class ReliableUnicastReceiver implements Runnable {
    private static int BUFFER_SIZE = 4096;
    private static ReliableUnicastReceiver instance = new ReliableUnicastReceiver();
    private ReliableUnicastSender sender = ReliableUnicastSender.getInstance();
    private int port;
    private Map<Integer, Hashtable<Integer,Message>> messageBufferTable;
    private Map<Integer, Integer> nextSequenceTable;

    DatagramSocket socket;

    public static ReliableUnicastReceiver getInstance(){
        return instance;
    }

    private ReliableUnicastReceiver(){
        messageBufferTable = new Hashtable<Integer, Hashtable<Integer, Message>>();
        nextSequenceTable = new Hashtable<Integer, Integer>();
        try {
            this.socket  = new DatagramSocket();
            this.port = socket.getLocalPort();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void renew(){
        messageBufferTable = new Hashtable<Integer, Hashtable<Integer, Message>>();
        nextSequenceTable = new Hashtable<Integer, Integer>();
        try {
            this.socket  = new DatagramSocket();
            this.port = socket.getLocalPort();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        byte[] buffer;
        byte[] data;
        Message message;
        Member member;
        DatagramPacket packet;
        MemberIndexer memberIndexer = MemberIndexer.getInstace();
        int senderId;
        while (true){
            buffer = new byte[BUFFER_SIZE];
            packet = new DatagramPacket(buffer, buffer.length);
            try {
                socket.receive(packet);
                data = Arrays.copyOf(packet.getData(), packet.getLength());
                packet.setLength(packet.getLength());
                message = Message.deserialize(data);
                senderId = message.getId();

                //System.out.println(message + " " + senderId);

                // This is special, because other dispatches could be handled later,
                // But the memberTable must be updated first.
                if ((message.getContent() != null)
                        && (message.getContent().getContent() instanceof GroupInfoMessage)
                        && ((GroupInfoMessage) message.getContent().getContent()).getAction().equals("ID_Allocation")){
                    GroupInfoMessage groupInfoMessage = (GroupInfoMessage) message.getContent().getContent();
                    List<Member> members = groupInfoMessage.getMembers();
                    memberIndexer.addMembers(members);
                    if (Client.getInstance().getId() == -1){
                        Client.getInstance().setId(groupInfoMessage.getYour_id());
                        Client.getInstance().enterOutput();
                    }
                    Client self = Client.getInstance();
                    if (self.getOrderType() == orderType.TotalOrder){
                        TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                        totalOrderMulticast.setNextExpectedSequence();
                    }
                    if (self.isLeader()){
                        TotalOrderSequencer totalOrderSequencer = TotalOrderSequencer.getInstance();
                        totalOrderSequencer.setNextMessageSequence();
                    }
                }

                member = memberIndexer.getById(senderId);

                //System.out.println(message.getAction() + " sender: " + senderId + " seq: " + message.getSequence());

                if (message.getAction().equals("delivery")){
                    int nextReceiveSequence;
                    Map<Integer, Message> messageBuffer;

                    if (!messageBufferTable.containsKey(senderId)){
                        messageBufferTable.put(senderId, new Hashtable<Integer, Message>());
                    }
                    if (!nextSequenceTable.containsKey(senderId)){
                        nextSequenceTable.put(senderId, 0);
                    }

                    messageBuffer = messageBufferTable.get(senderId);
                    nextReceiveSequence = nextSequenceTable.get(senderId);

                    // send ACK
                    sender.sendAck(message, member);

                    if (message.getSequence() >= nextReceiveSequence){
                        if (message.getSequence() == nextReceiveSequence){
                            // Delivery
                            this.delivery(message);
                            nextReceiveSequence += 1;
                            while (messageBuffer.containsKey(nextReceiveSequence)){
                                // Delivery
                                this.delivery(messageBuffer.get(nextReceiveSequence));
                                messageBuffer.remove(nextSequenceTable);
                                nextReceiveSequence++;
                            }
                        }
                        else {
                            if (!messageBuffer.containsKey(message.getSequence())){
                                messageBuffer.put(message.getSequence(), message);
                            }
                        }
                    }

                    nextSequenceTable.put(senderId, nextReceiveSequence);
                }
                else if (message.getAction().equals("ack")){
                    // Ack message
                    sender.ack(message);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void delivery(Message message){
        //System.out.println(message.getAction() + " sender: " + message.getId() + " seq: " + message.getSequence());
        GeneralMulticast.getInstance().delivery(message.getContent());
    }

    public int getPort() {
        return port;
    }
}
