package DT017A.Lab.Communication;

import DT017A.Lab.Model.Member;

import java.util.TimerTask;

/**
 * Created by tianyu on 31/12/14.
 */
public class Retransmission extends TimerTask {
    private int sequence;
    private Member member;
    private ReliableUnicastSender owner;

    public Retransmission(int sequence, Member member, ReliableUnicastSender owner){
        this.sequence = sequence;
        this.member = member;
        this.owner = owner;
    }

    public int getSequence(){
        return this.sequence;
    }

    public Member getMember(){
        return this.member;
    }

    @Override
    public void run() {
        //System.out.println("Seq: " + sequence + "Member_ID: " + member.getId());
        owner.resend(this);
    }
}
