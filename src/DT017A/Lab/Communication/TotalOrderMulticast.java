package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.MemberIndexer;
import DT017A.Lab.Model.Message.IMessage;
import DT017A.Lab.Model.Message.TotalOrderMessage;

import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

/**
 * Created by tianyu on 04/01/15.
 */
public class TotalOrderMulticast {
    private static TotalOrderMulticast instance = new TotalOrderMulticast();
    private  GeneralMulticast generalMulticast;
    private Client self;
    private Map<String, String> holdBackMessageMap;
    private Map<Integer, String> orderMessageMap;
    private final Object mutex;
    private int nextExpectedSequence;

    private TotalOrderMulticast(){
        generalMulticast = GeneralMulticast.getInstance();
        self = Client.getInstance();
        holdBackMessageMap = new HashMap<String, String>();
        orderMessageMap = new HashMap<Integer, String>();
        mutex = new Object();
        nextExpectedSequence = 0;
        new MessageWaitTask(this);
    }

    public Map<Integer, String> getOrderMessageMap() {
        return orderMessageMap;
    }

    public void setOrderMessageMap(Map<Integer, String> orderMessageMap) {
        this.orderMessageMap = orderMessageMap;
    }

    public void setHoldBackMessageMap(Map<String, String> holdBackMessageMap) {
        this.holdBackMessageMap = holdBackMessageMap;
    }

    public static TotalOrderMulticast getInstance() {
        return instance;
    }

    public void send(String chatText){
        TotalOrderMessage totalOrderMessage = new TotalOrderMessage();
        totalOrderMessage.setAction("ASK_SEQ");
        totalOrderMessage.setMessageId();
        totalOrderMessage.setText(chatText);
        this.send(totalOrderMessage);
    }

    private void send(IMessage message){
        TotalOrderMessage totalOrderMessage = (TotalOrderMessage)message;
        this.delivery(message);
        generalMulticast.send(totalOrderMessage);
        //MemberIndexer.getInstace().printCurrentTable();
    }

    public void delivery(IMessage message){
        TotalOrderMessage totalOrderMessage = (TotalOrderMessage)message;
        String text = totalOrderMessage.getText();
        String action = totalOrderMessage.getAction();
        String messageId = totalOrderMessage.getMessageId();
        int sequence = totalOrderMessage.getSequence();
        synchronized (mutex){
            if (action.equals("ASK_SEQ") && sequence == -1){
                if (self.isLeader()){
                    TotalOrderSequencer sequencer = TotalOrderSequencer.getInstance();
                    sequencer.reply(totalOrderMessage);
                }
                holdBackMessageMap.put(messageId, text);
                //System.out.println("ASK_SEQ: " + holdBackMessageMap);
                //System.out.println("Next expected seq: " + nextExpectedSequence);
            }
            else if (action.equals("ORDER") && sequence != -1){
                orderMessageMap.put(sequence, messageId);
                //System.out.println("ORDER" + orderMessageMap);
            }
        }
    }

    public void waitNextMessage(){
        synchronized (mutex){
            if (!orderMessageMap.containsKey(nextExpectedSequence)){
                return;
            }
            String nextExpectedMessageId = orderMessageMap.get(nextExpectedSequence);
            if (holdBackMessageMap.containsKey(nextExpectedMessageId)){
                String text = holdBackMessageMap.get(nextExpectedMessageId);
                self.outputText(text);
                orderMessageMap.remove(nextExpectedSequence);
                holdBackMessageMap.remove(nextExpectedMessageId);
                nextExpectedSequence += 1;
            }
        }
    }

    public void setNextExpectedSequence() {
        this.nextExpectedSequence = 0;
    }

    public void setNextExpectedSequence(int nextExpectedSequence) {
        this.nextExpectedSequence = nextExpectedSequence;
    }
}
