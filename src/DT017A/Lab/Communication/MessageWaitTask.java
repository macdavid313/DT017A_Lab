package DT017A.Lab.Communication;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tianyu on 04/01/15.
 */
public class MessageWaitTask extends TimerTask {
    private TotalOrderMulticast owner;
    private Timer timer;
    private static int PERIOD_TIME = 1000;

    public MessageWaitTask(TotalOrderMulticast owner){
        this.owner = owner;
        timer = new Timer();
        timer.schedule(this, 0, PERIOD_TIME);
    }

    @Override
    public void run() {
        if (owner != null){
            //System.out.println("RUNNING!");
            owner.waitNextMessage();
        }
    }
}
