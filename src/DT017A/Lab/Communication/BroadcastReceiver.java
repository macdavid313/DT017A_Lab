package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.*;
import DT017A.Lab.Model.Message.BroadcastMessage;
import DT017A.Lab.Model.Message.GroupInfoMessage;
import DT017A.Lab.Model.Message.Message;
import DT017A.Lab.orderType;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tianyu on 01/01/15.
 */
public class BroadcastReceiver implements Runnable{
    private static BroadcastReceiver instance = new BroadcastReceiver();
    private static int BUFFER_SIZE = 4096;
    private DatagramSocket socket;
    private volatile Thread blinker;

    public static BroadcastReceiver getInstance(){
        return instance;
    }

    public Thread getBlinker() {
        return blinker;
    }

    public void setBlinker(Thread blinker) {
        this.blinker = blinker;
    }

    private BroadcastReceiver(){
        try {
            this.socket = new DatagramSocket(Client.BROADCAST_RECEIVE_PORT);
            this.socket.setReuseAddress(true);
            //System.out.println("Success binding!");
        } catch (SocketException e) {
            //instance = new BroadcastReceiver();
            e.printStackTrace();
            //System.out.println("UNSuccess binding!");
        }
    }

    public void start(){
        this.blinker = new Thread(this);
        this.blinker.start();
    }

    public void stop(){
        blinker = null;
    }

    public void clear(){
        //this.socket
        this.socket.close();
        System.out.println("Socket Closed！");
    }

    @Override
    public void run() {
        byte[] data = new byte[BUFFER_SIZE];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        Message message;
        Thread thisThread = Thread.currentThread();
        while (blinker == thisThread){
            try {
                socket.receive(packet);
                data = Arrays.copyOf(packet.getData(), packet.getLength());
                packet.setLength(packet.getLength());
                message = Message.deserialize(data);
                if (message.getAction().equals("delivery")) delivery(message);
            } catch (SocketException e) {
                e.printStackTrace();
                //System.out.println("Inside the thread!");
                this.stop();
                this.clear();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
            //System.out.println("Running!");
        }
    }

    public synchronized void delivery(Message message){
        BroadcastMessage broadcastMessage =
                (BroadcastMessage)(message.getContent().getContent());
        Client self = Client.getInstance();
        String groupname = broadcastMessage.getGroupname();
        Member requester = broadcastMessage.getRequester();
        MemberIndexer memberIndexer = MemberIndexer.getInstace();
        if (self.isLeader() && self.getGroupname().equals(groupname)
                && !self.getUsername().equals(requester.getUsername())){
            // I am in the same chat room with you and I'm the leader
            memberIndexer.addMember(requester); // Side Effect
            List<Member> allMembers = memberIndexer.getMemberList();
            GroupInfoMessage groupInfoMessage = new GroupInfoMessage(allMembers);
            groupInfoMessage.setAction("ID_Allocation");
            groupInfoMessage.setSenderId(self.getId());
            groupInfoMessage.setYour_id(requester.getId());
            GeneralMulticast.getInstance().send(groupInfoMessage);
            if (self.getOrderType() == orderType.TotalOrder){
                TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                totalOrderMulticast.setNextExpectedSequence();
            }
            if (self.isLeader()){
                TotalOrderSequencer totalOrderSequencer = TotalOrderSequencer.getInstance();
                totalOrderSequencer.setNextMessageSequence();
            }
        }
    }

}
