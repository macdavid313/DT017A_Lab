package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.Member;
import DT017A.Lab.Model.MemberIndexer;
import DT017A.Lab.Model.Message.GeneralMulticastMessage;
import DT017A.Lab.Model.Message.GroupInfoMessage;
import DT017A.Lab.Model.Message.Message;
import DT017A.Lab.orderType;

import java.io.IOException;
import java.net.*;
import java.util.*;

/**
 * Created by tianyu on 31/12/14.
 */
public class ReliableUnicastSender {
    private int EXPIRE_TIME = 2000;
    private int delayTime = 1000;
    private static ReliableUnicastSender instance = new ReliableUnicastSender();
    DatagramSocket socket;
    // member-id -> (message-seq -> timer)
    private Map<Integer, Hashtable<Integer, Timer>> timerTable;
    // member-id -> (message-seq -> message)
    private Map<Integer, Hashtable<Integer, Message>> cachedMessages;
    // member-id -> (message-seq -> timer-task)
    private Map<Integer, Hashtable<Integer, TimerTask>> cachedRetransmission;
    // member-id -> (message-seq -> count)
    private Map<Integer, Hashtable<Integer, Integer>> msgRetransmissionCounter;
    // member-id -> message-seq
    private Map<Integer, Integer> nextSendSequence;
    private final Object mutex;
    private Client self;
    private Random rand;

    public static ReliableUnicastSender getInstance(){
        return instance;
    }

    private ReliableUnicastSender(){
        // Mutex_Lock
        mutex = new Object();
        timerTable =  new Hashtable<Integer, Hashtable<Integer, Timer>>();
        cachedMessages = new Hashtable<Integer, Hashtable<Integer, Message>>();
        cachedRetransmission = new Hashtable<Integer, Hashtable<Integer, TimerTask>>();
        msgRetransmissionCounter = new Hashtable<Integer, Hashtable<Integer, Integer>>();
        nextSendSequence = new Hashtable<Integer, Integer>();
        self = Client.getInstance();
        rand = new Random();
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void renew(){
        //this.mutex = new Object();
        this.timerTable =  new Hashtable<Integer, Hashtable<Integer, Timer>>();
        this.cachedMessages = new Hashtable<Integer, Hashtable<Integer, Message>>();
        this.cachedRetransmission = new Hashtable<Integer, Hashtable<Integer, TimerTask>>();
        this.msgRetransmissionCounter = new Hashtable<Integer, Hashtable<Integer, Integer>>();
        this.nextSendSequence = new Hashtable<Integer, Integer>();
        this.self = Client.getInstance();
        try {
            this.socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void send(Message message, Member member){
        Timer timer;
        TimerTask timerTask;
        InetAddress address;
        byte[] data;
        int memberId = member.getId();
        int sendSequence;
        DatagramPacket sendPacket;
        synchronized (mutex){
            if (!nextSendSequence.containsKey(memberId)){
                nextSendSequence.put(memberId, 0);
            }
            if (!timerTable.containsKey(memberId)){
                timerTable.put(memberId, new Hashtable<Integer, Timer>());
            }
            if (!cachedMessages.containsKey(memberId)){
                cachedMessages.put(memberId, new Hashtable<Integer, Message>());
            }
            if (!cachedRetransmission.containsKey(memberId)){
                cachedRetransmission.put(memberId, new Hashtable<Integer, TimerTask>());
            }
            if (!msgRetransmissionCounter.containsKey(memberId)){
                msgRetransmissionCounter.put(memberId, new Hashtable<Integer, Integer>());
            }
            sendSequence = nextSendSequence.get(memberId);
            try {
                message.setSequence(sendSequence);
                data = message.serialize();
                address = InetAddress.getByName(member.getIp()); // UnknownHostException
                sendPacket = new DatagramPacket(data, data.length, address, member.getPort());

                timer = new Timer();
                timerTask = new Retransmission(sendSequence, member, this);

                cachedMessages.get(memberId).put(sendSequence, message);
                timerTable.get(memberId).put(sendSequence, timer);
                cachedRetransmission.get(memberId).put(sendSequence, timerTask);
                msgRetransmissionCounter.get(memberId).put(sendSequence, 0);

                // Send
                socket.send(sendPacket); // IOException

                // Handle retransmission
                timer.schedule(timerTask, EXPIRE_TIME);
                nextSendSequence.put(memberId, sendSequence + 1);

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void resend(Retransmission task){
        Timer timer;
        Message message;
        Member member;
        int sequence;
        byte[] data;
        DatagramPacket sendPacket;
        InetAddress address;
        TimerTask timerTask;
        MemberIndexer memberIndexer = MemberIndexer.getInstace();

        member = task.getMember();
        sequence = task.getSequence();

        int count = msgRetransmissionCounter.get(member.getId()).get(sequence);
        if (count == 100){
            System.out.println("Retransmit too much times!");
            cachedRetransmission.get(member.getId()).remove(sequence);
            msgRetransmissionCounter.get(member.getId()).remove(sequence);
            return;
        }
        else if (count == 30 && member.isLeader()){
            // Leader is down! Start an election!
            //System.out.println("Start an election!");
            if (memberIndexer.getById(member.getId()) == null){
                cachedMessages.remove(member.getId());
                msgRetransmissionCounter.remove(member.getId());
                return;
            }
            else {
                GroupInfoMessage groupInfoMessage = new GroupInfoMessage();
                groupInfoMessage.setAction("LEADER_IS_DOWN");
                groupInfoMessage.setSenderId(self.getId());
                int new_leader = memberIndexer.electNewLeader();
                groupInfoMessage.setYour_leader(new_leader);
                GeneralMulticast generalMulticast = GeneralMulticast.getInstance();
                generalMulticast.send(groupInfoMessage);
                // Deliver to itself
                GeneralMulticastMessage generalMulticastMessage = new GeneralMulticastMessage();
                generalMulticastMessage.setContent(groupInfoMessage);
                generalMulticast.delivery(generalMulticastMessage);
                if (self.getOrderType() == orderType.TotalOrder){
                    TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                    totalOrderMulticast.setNextExpectedSequence();
                }
                if (self.isLeader()){
                    TotalOrderSequencer totalOrderSequencer = TotalOrderSequencer.getInstance();
                    totalOrderSequencer.setNextMessageSequence();
                }
                return;
            }
        }

        synchronized (mutex){
            message = cachedMessages.get(member.getId()).get(sequence);
            timer = timerTable.get(member.getId()).get(sequence);

            if (timer == null) return;

            task.cancel();
            timer.cancel();
            timer.purge();
            timer = new Timer();
            timerTask = new Retransmission(sequence, member, this);
            data = message.serialize();

            try {
                address = InetAddress.getByName(member.getIp());
                sendPacket = new DatagramPacket(data, data.length, address, member.getPort());
                timerTable.get(member.getId()).remove(sequence);
                timerTable.get(member.getId()).put(sequence, timer);
                cachedRetransmission.get(member.getId()).put(sequence, timerTask);

                Timer delayTimer = new Timer();
                TimerTask delaySender = new DelaySender(socket, sendPacket);
                delayTimer.schedule(delaySender, delayTime);

                timer.schedule(timerTask, EXPIRE_TIME); // resend

                msgRetransmissionCounter.get(member.getId()).remove(sequence);
                msgRetransmissionCounter.get(member.getId()).put(sequence, count+1);

                System.out.println(cachedRetransmission);
                System.out.println("Counter: " + msgRetransmissionCounter.get(member.getId()).get(sequence));

            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    public void ack(Message message){
        Timer timer;
        TimerTask task;
        int memberId = message.getId();
        int sequence = message.getSequence();
        synchronized (mutex){
            timer = timerTable.get(memberId).get(sequence);
            task = cachedRetransmission.get(memberId).get(sequence);
            if (timer != null){
                task.cancel();
                timer.cancel();
                timer.purge();
                timerTable.get(memberId).remove(sequence);
                cachedMessages.get(memberId).remove(sequence);
                cachedRetransmission.get(memberId).remove(sequence);
            }
        }
    }

    public void sendAck(Message msg, Member member) throws UnknownHostException {
        double rate = rand.nextDouble();
        if (rate >= self.getDropRate()){
            Message message = new Message(msg);
            message.setId(Client.getInstance().getId());
            message.setAction("ack");
            byte[] data = message.serialize();
            InetAddress address = InetAddress.getByName(member.getIp());
            DatagramPacket sendPacket = new DatagramPacket(data, data.length, address, member.getPort());
            Timer delayTimer = new Timer();
            TimerTask delaySender = new DelaySender(socket, sendPacket);
            delayTimer.schedule(delaySender, delayTime);
        }
        else {
            System.out.println("A message has been dropped.");
        }
    }
}
