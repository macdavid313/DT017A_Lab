package DT017A.Lab.Communication;

import DT017A.Lab.Client;
import DT017A.Lab.Model.Message.TotalOrderMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tianyu on 04/01/15.
 */
public class TotalOrderSequencer {
    private static TotalOrderSequencer instance = new TotalOrderSequencer();
    private GeneralMulticast generalMulticast = GeneralMulticast.getInstance();
    private int nextMessageSequence;

    private TotalOrderSequencer(){
        this.nextMessageSequence = 0;
    }

    public static TotalOrderSequencer getInstance() {
        return instance;
    }

    public void reply(TotalOrderMessage message){
        TotalOrderMessage reply =
                new TotalOrderMessage(message.getText(), message.getMessageId());
        reply.setAction("ORDER");
        reply.setSequence(nextMessageSequence);
        if (Client.getInstance().isLeader()){
            TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
            Map<Integer, String> tmp = totalOrderMulticast.getOrderMessageMap();
            tmp.put(nextMessageSequence, message.getMessageId());
            totalOrderMulticast.setOrderMessageMap(tmp);
        }
        generalMulticast.send(reply);
        nextMessageSequence++;
    }

    public TotalOrderMessage replySelf(TotalOrderMessage message){
        TotalOrderMessage reply =
                new TotalOrderMessage(message.getText(), message.getMessageId());
        reply.setAction("ORDER");
        reply.setSequence(nextMessageSequence);
        nextMessageSequence++;
        return reply;
    }

    public void setNextMessageSequence() {
        this.nextMessageSequence = 0;
    }

    public void setNextMessageSequence(int nextMessageSequence) {
        this.nextMessageSequence = nextMessageSequence;
    }
}
