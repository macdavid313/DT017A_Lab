package DT017A.Lab.Model.Message;

import DT017A.Lab.Model.Member;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tianyu on 01/01/15.
 */
public class GroupInfoMessage implements IMessage, Serializable {
    private int your_id = -1;
    private int your_leader = -1;
    private int senderId;
    private List<Member> members;
    private String action = null;

    public GroupInfoMessage(){

    }

    public GroupInfoMessage(List<Member> members){
        this.members = members;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public int getYour_id() {
        return your_id;
    }

    public void setYour_id(int id){
        this.your_id = id;
    }

    public int getYour_leader() {
        return your_leader;
    }

    public void setYour_leader(int your_leader) {
        this.your_leader = your_leader;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
