package DT017A.Lab.Model.Message;

import DT017A.Lab.Client;
import DT017A.Lab.Model.MemberIndexer;

import java.io.Serializable;

/**
 * Created by tianyu on 04/01/15.
 */
public class CausalOrderMessage implements IMessage, Serializable {
    private Integer[] vectorClock;
    private String text;
    private Integer source;

    public Integer[] getVectorClock() {
        return vectorClock;
    }

    public void setVectorClock(Integer[] vectorClock) {
        this.vectorClock = vectorClock;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        Client self = Client.getInstance();
        String name = self.getUsername();
        this.text = name + " says: " + text + "\n";
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }
}
