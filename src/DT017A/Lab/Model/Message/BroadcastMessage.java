package DT017A.Lab.Model.Message;

import DT017A.Lab.Client;
import DT017A.Lab.Model.Member;

import java.io.Serializable;

/**
 * Created by tianyu on 01/01/15.
 */
public class BroadcastMessage implements IMessage, Serializable {
    private String groupname;
    private Member requester;

    public BroadcastMessage(){
        this.groupname = Client.getInstance().getGroupname();
        this.requester = Client.getInstance().toMember();
    }

    public String getGroupname() {
        return groupname;
    }

    public Member getRequester() {
        return requester;
    }

    public void setRequester(Member requester) {
        this.requester = requester;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
}
