package DT017A.Lab.Model.Message;

import java.io.Serializable;

/**
 * Created by tianyu on 01/01/15.
 */
public class GeneralMulticastMessage implements Serializable {
    private IMessage content;

    public IMessage getContent() {
        return content;
    }

    public void setContent(IMessage content) {
        this.content = content;
    }
}
