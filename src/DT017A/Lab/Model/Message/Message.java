package DT017A.Lab.Model.Message;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.*;

/**
 * Created by tianyu on 30/12/14.
 */

@XmlRootElement(name = "Message")
public class Message implements Serializable {
    @XmlElement(name = "ID")
    private int id;

    @XmlElement(name = "Content")
    private GeneralMulticastMessage content;

    @XmlElement(name="Action")
    private String action;

    @XmlElement(name = "Sequence")
    private int sequence;

    public Message(){

    }

    public Message(Message message){
        this.id = message.getId();
        this.action = message.getAction();
        this.sequence = message.getSequence();
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public GeneralMulticastMessage getContent() {
        return content;
    }

    public void setContent(GeneralMulticastMessage content) {
        this.content = content;
    }

    public String getAction(){
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getSequence(){
        return this.sequence;
    }

    public void setSequence(int sequence){
        this.sequence = sequence;
    }

    public byte[] serialize(){
        ObjectOutputStream out = null;
        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();
            out = new ObjectOutputStream(bos);
            out.writeObject(this);
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Message deserialize(byte[] msg){
        ByteArrayInputStream bis = null;
        ObjectInput in = null;
        try {
            bis = new ByteArrayInputStream(msg);
            in = new ObjectInputStream(bis);
            return (Message)in.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
