package DT017A.Lab.Model.Message;

import DT017A.Lab.Client;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by tianyu on 04/01/15.
 */
public class TotalOrderMessage implements IMessage, Serializable {
    private String text;
    private String action = null;
    private String messageId;
    private int sequence = -1;

    public TotalOrderMessage(){

    }

    public TotalOrderMessage(String text, String messageId){
        this.text = text;
        this.messageId = messageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        Client self = Client.getInstance();
        String name = self.getUsername();
        this.text = name + " says: " + text + "\n";
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId() {
        Client self = Client.getInstance();
        String name = self.getUsername();
        this.messageId = name + "@" + new Timestamp(System.currentTimeMillis()).toString();
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }
}
