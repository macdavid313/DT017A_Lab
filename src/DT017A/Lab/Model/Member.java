package DT017A.Lab.Model;

import DT017A.Lab.Client;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by tianyu on 31/12/14.
 */
public class Member implements Serializable {
    private String username;
    private String groupname;
    private boolean isLeader = false;
    private int id = - 1;
    private String ip;
    private int port;

    public Member(String groupname){
        this.groupname = groupname;
        this.username = new Timestamp(System.currentTimeMillis()).toString();
    }

    public Member(String groupname, String username, boolean isLeader, int id, String ip, int port){
        this.groupname = groupname;
        this.username = username;
        this.isLeader = isLeader;
        this.id = id;
        this.ip = ip;
        this.port = port;
    }

    public int getId(){
        return this.id;
    }

    public String getUsername(){
        return this.username;
    }

    public String getGroupname(){
        return this.groupname;
    }

    public boolean isLeader() {
        return isLeader;
    }

    public void setLeader(boolean isLeader) {
        this.isLeader = isLeader;
    }

    public String getIp(){
        return this.ip;
    }

    public void setIp(String ip){
        this.ip = ip;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getPort(){
        return port;
    }
}
