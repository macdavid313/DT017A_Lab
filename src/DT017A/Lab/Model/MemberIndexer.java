package DT017A.Lab.Model;

import DT017A.Lab.Client;
import DT017A.Lab.Communication.BroadcastReceiver;

import java.util.*;

/**
 * Created by tianyu on 31/12/14.
 */
public class MemberIndexer {
    private Map<Integer, Member> memberTable;
    private static MemberIndexer instance = new MemberIndexer();
    int leader = -1;
    private Client self = Client.getInstance();

    private MemberIndexer(){
        memberTable = new Hashtable<Integer, Member>();
    }

    public static MemberIndexer getInstace(){
        return instance;
    }

    public void renew(){
        this.leader = -1;
        this.memberTable = null;
        this.memberTable = new Hashtable<Integer, Member>();
    }

    public Map<Integer, Member> getAllMembers(){
        return memberTable;
    }

    public void addMember(Client self){
        memberTable.put(self.getId(), self.toMember());
        //self.enterOutput();
        setLeader();
        syncToSelf();
    }

    public synchronized void addMember(Member member){
        if (!memberTable.containsKey(member.getId())){
            if (member.getId() == -1){
                int index = this.find_max_index() + 1;
                member.setId(index); // Side Effect
                memberTable.put(index, member);
                self.memberJoinedOutput(member);
            }
            else {
                memberTable.put(member.getId(), member);
                self.memberJoinedOutput(member);
            }
        }
        this.removeUnRegisteredMember();
        setLeader();
        //syncToSelf();
    }

    public void addMembers(List<Member> members){
        for (Member member : members){
            this.addMember(member);
        }
        //this.removeUnRegisteredMember();
    }

    public void removeMember(int id){
        if (memberTable.containsKey(id)){
            Member member = this.getById(id);
            self.memberLeftOutput(member);
            memberTable.remove(id);
        }
    }

    public void removeMember(Member member){
        if (memberTable.containsKey(member.getId())){
            self.memberLeftOutput(member);
            memberTable.remove(member.getId());
        }
    }

    public synchronized int find_max_index(){
        if (memberTable.isEmpty()){
            return -1;
        }
        else {
            Set<Integer> keys = memberTable.keySet();
            int max = -1;
            for (int i : keys){
                if (max < i){
                    max = i;
                }
            }
            return max;
        }
    }

    public synchronized int electNewLeader(){
        memberTable.remove(leader);
        if (memberTable.isEmpty()){
            return -1;
        }
        else {
            int max = find_max_index();
            setLeader(max);
            return this.leader;
        }
    }

    public synchronized int countMembers(){
        return memberTable.size();
    }

    public Member getById(int id){
        return memberTable.get(id);
    }

    public Member getByName(String name){
        for (Map.Entry<Integer, Member> entry : this.memberTable.entrySet()){
            if (this.self.getUsername().equals(entry.getValue().getUsername())){
                return entry.getValue();
            }
        }
        return null;
    }

    public int getLeader() {
        return leader;
    }

    public synchronized void setLeader() {
        for (Map.Entry<Integer, Member> entry : this.memberTable.entrySet()){
            if (entry.getValue().isLeader()){
                this.leader = entry.getKey();
                return;
            }
        }
        this.leader = -1;
    }

    public synchronized void setLeader(int leader) {
        this.leader = leader;
        if (!getById(leader).isLeader()){
            getById(leader).setLeader(true);
        }
        //this.syncToSelf();
        if (leader == self.getId()){
            self.setLeader(true);
            BroadcastReceiver broadcastReceiver = BroadcastReceiver.getInstance();
            broadcastReceiver.stop();
            broadcastReceiver.start();
            this.self.setMsg_pool("Now you are the leader, id = " + self.getId() + "\n");
        }
    }

    // Just used for debugging
    public void printCurrentTable(){
        System.out.println("***********************************");
        for (Map.Entry<Integer, Member> entry : this.memberTable.entrySet()){
            Member member = entry.getValue();
            System.out.println("ID: " + member.getId() + "\n"
                                + "Name: " + member.getUsername() + "\n"
                                + "Leader?" + member.isLeader() + "\n"
                                + "IP: " + member.getIp() + "\n");
        }
        System.out.println("***********************************");
    }

    public Member findLeader(){
        this.setLeader();
        if (this.leader == -1){
            System.out.println("Error: Leader not found!");
            return null;
        } else {
            return this.getById(this.leader);
        }
    }

    public String groupInfo(){
        Client self = Client.getInstance();
        Member leader = this.findLeader();
        return
                "Group: " + self.getGroupname() + ".\n"
                + this.countMembers() + " people online.\n"
                + "Group's Leader: ID: " + leader.getId() + " "
                + "Name: " + leader.getUsername() + "\n";
    }

    public synchronized List<Member> getMemberList(){
        List<Member> members = new ArrayList<Member>();
        for (Map.Entry<Integer, Member> entry : this.memberTable.entrySet()){
            members.add(entry.getValue());
        }
        return members;
    }

    public synchronized void removeUnRegisteredMember(){
        //printCurrentTable();
        for (Map.Entry<Integer, Member> entry : this.memberTable.entrySet()){
            if (entry.getKey() == -1){
                this.memberTable.remove(entry.getKey());
            }
        }
    }

    private void syncToSelf(){
        Member self = getByName(this.self.getUsername());
        this.self.setLeader(self.isLeader());
        this.self.setId(self.getId());
        if (this.self.isLeader()){
            // Find itself is the Leader.
            BroadcastReceiver broadcastReceiver = BroadcastReceiver.getInstance();
            broadcastReceiver.start();
            this.self.setMsg_pool("Now you are the leader, id = " + self.getId() + "\n");
        }
        //printCurrentTable();
    }
}
