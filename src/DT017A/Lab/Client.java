package DT017A.Lab;

import DT017A.Lab.Model.Member;
import DT017A.Lab.Model.MemberIndexer;

import javax.swing.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;

/**
 * Created by tianyu on 31/12/14.
 */
public class Client {
    private static Client instance = new Client();
    private String username = null;
    private String groupname;
    private boolean isLeader;
    private orderType orderType;
    private int id = -1;
    private String ip;
    private int port;
    public static int BROADCAST_RECEIVE_PORT = 45678;
    private JTextArea msg_pool;
    private float dropRate;

    public static Client getInstance(){
        return instance;
    }

    public void init(String groupname, orderType orderType, JTextArea msg_pool, int port){
        this.groupname = groupname;
        this.orderType = orderType;
        this.msg_pool = msg_pool;
        this.port = port;
        this.setUsername();
        this.setIp();
        this.msg_pool = msg_pool;
        // if it's a new chat_room, id = 100; else id will be allocated by leader.
    }

    public void destruct(){
        username = null;
        groupname = null;
        orderType = null;
        id = -1;
        ip = null;
        port = -1;
        msg_pool = null;
    }

    public Member toMember(){
        return new Member(groupname, username, isLeader, id, ip, port);
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(){
        this.username = new Timestamp(System.currentTimeMillis()).toString();
    }

    public String getGroupname(){
        return this.groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public boolean isLeader() {
        return isLeader;
    }

    public void setLeader(boolean isLeader) {
        this.isLeader = isLeader;
    }

    public orderType getOrderType() {
        return orderType;
    }

    public void setOrderType(orderType orderType){
        this.orderType = orderType;
    }

    public String getIp(){
        return this.ip;
    }

    public void setIp(){
        try {
            this.ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            System.out.println("Get Local Host Failed.");
            e.printStackTrace();
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public JTextArea getMsg_pool(){
        return msg_pool;
    }

    public void setMsg_pool(JTextArea msg_pool){
        this.msg_pool = msg_pool;
    }

    public void setMsg_pool(String newline) {
        msg_pool.append(newline);
    }

    public float getDropRate() {
        return dropRate;
    }

    public void setDropRate(float dropRate) {
        this.dropRate = dropRate;
    }

    public void memberJoinedOutput(Member member){
        if (member.getId() != -1 && !member.getUsername().equals(this.username)){
            //System.out.println("With Client: " + this.getUsername() + member.getId() + " " + member.getUsername() + " joined");
            this.setMsg_pool(member.getUsername() + " has joined.\n");
        }
    }

    public void memberLeftOutput(Member member){
        if (member.getId() != -1 && !member.getUsername().equals(this.username)){
            this.setMsg_pool(member.getUsername() + " has just left.\n");
        }
    }

    public void enterOutput(){
        this.setMsg_pool("You " + "(" + id + ") has joined the Room_" + this.groupname + "\n");
    }

    public void outputText(String text){
        this.setMsg_pool(text);
    }
}

