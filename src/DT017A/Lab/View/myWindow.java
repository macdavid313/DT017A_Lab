package DT017A.Lab.View;

import javax.swing.*;
import java.awt.*;

import static javax.swing.BoxLayout.Y_AXIS;

/**
 * Created by tianyu on 27/11/14.
 */
public class myWindow extends JFrame{
    private String windowType;

    public myWindow(String windowType){
        this.windowType = windowType;

        this.setTitle("Distributed Chat Room");
        this.setVisible(true);
        this.setLocationRelativeTo(this.getRootPane());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        if(windowType.equalsIgnoreCase("chooseOrderWindow")){
            this.setLayout(new GridLayout(1, 2));
            this.setSize(480, 240);
            this.setResizable(false);
        }
        else if(windowType.equalsIgnoreCase("chatRoomWindow")){
            this.setSize(320, 720);
            this.setResizable(true);
        }
    }


}
