package DT017A.Lab.View;

import DT017A.Lab.Client;
import DT017A.Lab.Communication.*;
import DT017A.Lab.Model.MemberIndexer;
import DT017A.Lab.Model.Message.GroupInfoMessage;
import DT017A.Lab.orderType;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by tianyu on 27/11/14.
 */
public class chatRoomWindow extends myWindow{
    private JTextField groupName = new JTextField(24);
    private final JButton createGroup = new JButton("Create Group");
    private  final JButton joinGroup = new JButton("Join Group");
    private final JButton leaveGroup = new JButton("Leave Group");
    private JTextField msg = new JTextField(24);
    private final JButton sendMsg = new JButton("Send Chat Message");
    private JTextArea msg_pool = new JTextArea();

    private JPanel jpanel = new JPanel();
    private JScrollPane jScrollPane;

    Client self = Client.getInstance();
    MemberIndexer memberIndexer = MemberIndexer.getInstace();
    ReliableUnicastReceiver reliableUnicastReceiver = ReliableUnicastReceiver.getInstance();
    BroadcastSender broadcastSender = BroadcastSender.getInstance();
    GeneralMulticast generalMulticast = GeneralMulticast.getInstance();
    Thread unicastListener;

    public chatRoomWindow(final orderType orderType){
        super("chatRoomWindow");

        leaveGroup.setEnabled(false);
        sendMsg.setEnabled(false);

        jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));
        jpanel.add(groupName);
        jpanel.add(createGroup);
        jpanel.add(joinGroup);
        jpanel.add(leaveGroup);
        jpanel.add(msg);
        jpanel.add(sendMsg);

        msg_pool.setRows(24);
        jScrollPane = new JScrollPane(msg_pool);

        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        this.add(jpanel);
        this.add(jScrollPane);

        createGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String groupname = groupName.getText();
                msg_pool.setText(null);

                createGroup.setEnabled(false);
                joinGroup.setEnabled(false);
                leaveGroup.setEnabled(true);
                sendMsg.setEnabled(true);
                groupName.setEnabled(false);

                // Initialization
                self.destruct();
                memberIndexer.renew();

                // Initialization self
                self = Client.getInstance();
                // Including ip and username
                self.init(groupname, orderType, msg_pool, reliableUnicastReceiver.getPort());
                self.setId(0);
                self.setLeader(true);

                // Add self to the memberTable
                memberIndexer.addMember(self);
                self.enterOutput();

                // When a new member joins, (s)he will broadcast a message to find the leader,
                // and leader will answer back and allocate an ID for him.
                BroadcastReceiver broadcastReceiver = BroadcastReceiver.getInstance();
                broadcastReceiver.start();

                unicastListener = new Thread(reliableUnicastReceiver);
                unicastListener.start();
            }
        });

        joinGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String groupname = groupName.getText();
                msg_pool.setText(null);

                createGroup.setEnabled(false);
                joinGroup.setEnabled(false);
                leaveGroup.setEnabled(true);
                sendMsg.setEnabled(true);
                groupName.setEnabled(false);

                // Initialization
                ReliableUnicastSender.getInstance().renew();
                reliableUnicastReceiver.renew();
                self.destruct();
                memberIndexer.renew();

                // Self Initialization
                self = Client.getInstance();
                // Including ip and username
                self.init(groupname, orderType, msg_pool, reliableUnicastReceiver.getPort());
                self.setLeader(false);

                // Add self to the memberTable
                memberIndexer.addMember(self); // Note that the ID(default = -1) has not been allocated.

                unicastListener = new Thread(reliableUnicastReceiver);
                unicastListener.start();

                // Find leader by broadcasting a message
                broadcastSender.findLeader();
            }
        });

        leaveGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                groupName.setEnabled(true);
                createGroup.setEnabled(true);
                joinGroup.setEnabled(true);
                leaveGroup.setEnabled(false);
                sendMsg.setEnabled(false);

                GroupInfoMessage groupInfoMessage = new GroupInfoMessage();
                groupInfoMessage.setSenderId(self.getId());
                groupInfoMessage.setAction("Broadcast_Leaving");

                if (self.isLeader()){
                    groupInfoMessage.setYour_leader(memberIndexer.find_max_index());
                }

                //memberIndexer.printCurrentTable();

                if (self.isLeader()){
                    BroadcastReceiver broadcastReceiver = BroadcastReceiver.getInstance();
                    broadcastReceiver.stop();
                    //System.out.println("After press LEAVE button ***");
                    broadcastReceiver.clear();
                }

                generalMulticast.send(groupInfoMessage);

                //memberIndexer.printCurrentTable();
            }
        });

        sendMsg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String chatText = msg.getText();
                msg.setText(null);
                if (orderType == DT017A.Lab.orderType.TotalOrder){
                    TotalOrderMulticast totalOrderMulticast = TotalOrderMulticast.getInstance();
                    totalOrderMulticast.send(chatText);
                }
                else if (orderType == DT017A.Lab.orderType.CausalOrder){
                    CausalOrderMulticast causalOrderMulticast = CausalOrderMulticast.getInstance();
                    causalOrderMulticast.send(chatText);
                }
                //memberIndexer.printCurrentTable();
            }
        });
    }
}
