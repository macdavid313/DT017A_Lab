package DT017A.Lab.View;

import DT017A.Lab.orderType;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by tianyu on 27/11/14.
 */
public class chooseOrderWindow extends myWindow{
    private JButton total = new JButton("Total Order");
    private  JButton causal = new JButton("Causal Order");
    public chooseOrderWindow(){
        super("chooseOrderWindow");

        this.add(total);
        this.add(causal);

        total.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                new chatRoomWindow(orderType.TotalOrder);
                dispose();
            }
        });

        causal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                new chatRoomWindow(orderType.CausalOrder);
                dispose();
            }
        });
    }
}
